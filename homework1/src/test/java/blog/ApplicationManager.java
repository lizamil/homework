package blog;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import java.util.concurrent.TimeUnit;

public class ApplicationManager {
    EventFiringWebDriver driver;
    HelperBase helper;
    private String browser;

    public ApplicationManager(String browser) {
        this.browser = browser;
    }

    public HelperBase getHelperBase() {
        return helper;
    }

    public void init() {

        if (browser.equals(BrowserType.CHROME)) {
            driver = new EventFiringWebDriver(new ChromeDriver());
        }
        if (browser.equals(BrowserType.FIREFOX)) {
            driver = new EventFiringWebDriver(new FirefoxDriver());
        }
        if (browser.equals(BrowserType.EDGE)) {
            driver = new EventFiringWebDriver(new EdgeDriver());
        }


        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        helper = new HelperBase(driver);
        helper.openSite("http://192.168.247.5:8000/wp-login.php");
        helper.login("user", "password");
    }
    public void stop() {
        driver.quit();
    }



}
