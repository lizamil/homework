package blog;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HelperBase {
    WebDriver driver;


    public HelperBase(WebDriver driver) {
        this.driver = driver;
    }

    public void waitPresenceElement(By locator, int timeOut) {
        new WebDriverWait(driver, timeOut).until(ExpectedConditions.presenceOfElementLocated(locator));
    }
    public void type(By locator, String text) {
        if (text != null) {
            click(locator);
            driver.findElement(locator).clear();
            driver.findElement(locator).sendKeys(text);
        }
    }
    public void click(By locator) {
        waitPresenceElement(locator, 5);
        driver.findElement(locator).click();
    }
    public boolean isElementPresent(By locator) {
        return (driver.findElements(locator).size()>0);
    }

    public void openSite(String url) {
        driver.get(url);
    }
    public void login(String user, String password) {
        type(By.xpath(".//*[@id='user_login']"), "user");
        type(By.xpath(".//*[@id='user_pass']"), "password");
        click(By.xpath(".//*[@id='wp-submit']"));
    }
}
