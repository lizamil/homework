package blog;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;

public class Tests extends TestBase {

     @Test
    public void isDashboardPage() {
        Assert.assertTrue(app.helper.isElementPresent(By.xpath(".//h1 [text()='Dashboard']")));
        System.out.println("____________Dashboard________________");
    }

}
